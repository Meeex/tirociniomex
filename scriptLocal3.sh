#!bin/bash

cd /home/hotsquad/Desktop/Testsh/

for d1 in *; do    
    cd /home/hotsquad/Desktop/Testsh/$d1/
    for d2 in *; do
        cd /home/hotsquad/Desktop/Testsh/$d1/$d2/

        real=`ls | grep 155`
        real=${real%.*}
        r=`echo $real | cut -c12-20`

        tiny=`ls | grep tiny`
        tiny=${tiny%.*}
        t=`echo $tiny | cut -c23-30`

        full=`ls | grep yolo-predict`
        full=${full%.*}
        f=`echo $full | cut -c18-30`
    
        if [ $r -gt $t ]; then
            tinyP=$(($t*100))
            tinyP=$(($tinyP/$r)) 
        else
            tinyP=$(($r*100))
            tinyP=$(($tinyP/$t))
        fi

        if [ $r -gt $f ]; then
            fullP=$(($f*100))
            fullP=$(($fullP/$r)) 
        else
            fullP=$(($r*100))
            fullP=$(($fullP/$f))
        fi

        mv ./$tiny* ./$tiny-$tinyP%.jpg
        mv ./$full* ./$full-$fullP%.jpg

    done
done
