#!bin/bash

delta=12
i=0

scp -r hotsquad@10.0.0.35:Scrivania/count_people/RGBphotos /home/hotsquad/Desktop/i
scp -r pi@10.0.0.36:darknet-nnpack/RGBphotos /home/hotsquad/Desktop/k

cd ./i
mkdir ../Testsh

for f in *; do
	x=`echo "$f" | cut -c1-10`
    cd ../k
    for g in *; do
        y=`echo "$g" | cut -c1-10`

        x2=$(($x+$delta))
        x3=$(($x-$delta))

        if [ $y -lt $x2 ]; then 
            if [ $y -gt $x3 ]; then
                echo $y"-----Y"
                
                i=$(($i+1))
                
                mkdir ../Testsh/$i
                mkdir ../Testsh/$i/Intel
                mkdir ../Testsh/$i/Kinect
                cp ../i/$x* ../Testsh/$i/Intel/$x.png
                cp ../k/$y* ../Testsh/$i/Kinect/$y.ppm 
            fi
        fi
    done
done
