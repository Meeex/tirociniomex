#!bin/bash

cd /home/hotsquad/Desktop/Testsh/

IntelFull=0
IntelTiny=0
KinectFull=0
KinectTiny=0
i=0

for d1 in *; do    
    i=$(($i+1))

    cd /home/hotsquad/Desktop/Testsh/$d1/Intel/
    tiny=`ls | grep tiny`
    tiny=${tiny%\%.*}
    t=${tiny##*-}

    full=`ls | grep yolo-predict`
    full=${full%\%.*}
    f=${full##*-}

    IntelTiny=$(($IntelTiny+$t))
    IntelFull=$(($IntelFull+$f))

    cd /home/hotsquad/Desktop/Testsh/$d1/Kinect/
    tiny=`ls | grep tiny`
    tiny=${tiny%\%.*}
    t=${tiny##*-}

    full=`ls | grep yolo-predict`
    full=${full%\%.*}
    f=${full##*-}

    KinectTiny=$(($KinectTiny+$t))
    KinectFull=$(($KinectFull+$f))        

done

IntelFull=$(($IntelFull/$i))
IntelTiny=$(($IntelTiny/$i))
KinectFull=$(($KinectFull/$i))
KinectTiny=$(($KinectTiny/$i))

cd /home/hotsquad/Desktop/Testsh/
echo IntelFull","$IntelFull >> accuracy.csv
echo IntelTiny","$IntelTiny >> accuracy.csv
echo KinectFull","$KinectFull >> accuracy.csv
echo KinectTiny","$KinectTiny >> accuracy.csv
