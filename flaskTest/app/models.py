from datetime import datetime
from app import db
from flask_login import UserMixin
from app import login
from werkzeug.security import generate_password_hash
from werkzeug.security import check_password_hash
import time 

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))

    def __repr__(self):
        return '<User {}>'.format(self.username)
		
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)
		
    @login.user_loader
    def load_user(id):
        return User.query.get(int(id))
		
class Photo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    pathRGB = db.Column(db.String(140))
    pathPRD = db.Column(db.String(140))
    timestamp = db.Column(db.String(140), index=True, default=str(time.time()))
    camera_id = db.Column(db.Integer, db.ForeignKey('camera.id'))
    
    def set_timestamp(self, timestamp):
        self.timestamp = timestamp
	
    def set_path(self, path):
        self.path = path
    def __repr__(self):
        return '<Photos {}>'.format(self.pathRGB)


 
class Camera(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(140))
    aula_id = db.Column(db.Integer, db.ForeignKey('aula.id'))
    photos = db.relationship('Photo', backref='author', lazy='dynamic')
    ip = db.Column(db.String(140))
    state = db.Column(db.String(140),default="OFF")
    def __repr__(self):
        return '<Camera {} {} {} {}>'.format(self.nome, self.aula_id, self.id, self.state)
        
        
class Aula(db.Model):
    id = db.Column(db.Integer, primary_key=True,autoincrement=False)
    nome = db.Column(db.String(140))
    cameras = db.relationship('Camera', backref='author', lazy='dynamic')
  
    def __repr__(self):
        return '<Aula {}>'.format(self.nome)
        
    def has_camera(self, idCamera):
        return self.cameras.filter(cameras.id == idCamera).count() > 0
