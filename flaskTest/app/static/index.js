$(document).ready(function(){
	
	$("#immagini").on( "change", function(){
		value = $(this).children("option:selected").attr('id');
		$.get('/fetchCameras?aula='+value, {
			
        }).done(function(response) {
			string = "";
            for (var x in response.scores) {
				string += "<a href=dashBoard?camera="+response.scores[x][0]+"><button>"+response.scores[x][1]+"</button></a>";
            }
			$("#buttonContainerImgs").html(string);

        }).fail(function() {
            console.log("FALLITO");
        });
    });

    $("#grafici").on( "change", function(){
		value = $(this).children("option:selected").attr('id');
		$.get('/fetchCameras?aula='+value, {
			
        }).done(function(response) {
			string = "";
            for (var x in response.scores) {
				string += "<a href=graphs?camera="+response.scores[x][0]+"><button>"+response.scores[x][1]+"</button></a>";
            }
			$("#buttonContainerGraphs").html(string);

        }).fail(function() {
            console.log("FALLITO");
        });
    });
});

