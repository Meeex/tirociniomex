from app import app,db
from app.forms import LoginForm
from flask import render_template,request, flash, redirect, url_for, jsonify
from flask_login import current_user, login_user
from app.models import User, Aula, Photo, Camera
from flask_login import login_required
from werkzeug.security import check_password_hash
from flask import request
from werkzeug.urls import url_parse
from flask_login import logout_user
from app.forms import RegistrationForm
from datetime import datetime, date
import time 
import xml.etree.ElementTree as ET
import requests
import calendar
import collections
import csv
import subprocess
import os 

MAX_STORED_PHOTOS = 10

@app.route('/')
@app.route('/index')
@login_required
def index():
    user = {'username' : current_user.username}
    aule = db.session().query(Aula).all()
    camere = db.session().query()
    return render_template('index.html',title='home',aule=aule)
	
	
@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)
	
	
@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))
	
@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)
	

@app.route('/dashBoard', methods=['GET'])
def dashBoard():
    camera_id = request.args.get("camera")
    photo_id = request.args.get("photo")
    image=None
    value=None
    if camera_id is None and photo_id is None:
        print("Invalid Request, Please insert Camera")
    elif photo_id is None:
        images = db.session.query(Photo).filter_by(camera_id=camera_id).order_by(Photo.timestamp.desc()).limit(MAX_STORED_PHOTOS)
        image = db.session.query(Photo).filter_by(camera_id=camera_id).order_by(Photo.timestamp.desc()).first()
        camera = db.session.query(Camera).filter_by(id=camera_id).first()
        aula = db.session.query(Aula).filter_by(id=camera.aula_id).first()
    else:
        images = db.session.query(Photo).filter_by(camera_id=camera_id).order_by(Photo.timestamp.desc()).limit(MAX_STORED_PHOTOS)
        image = db.session.query(Photo).filter_by(id=photo_id).first()
        camera = db.session.query(Camera).filter_by(id=camera_id).first()
        aula = db.session.query(Aula).filter_by(id=camera.aula_id).first()
        
    
    with open("app/static/"+ str(aula.id) +"/"+ str(camera.nome) +"/letture.csv", "rt") as f:
        for row in csv.reader(f):
            if str(row[0]) == str(image.timestamp):
                value = row[1]
    
    date=datetime.utcfromtimestamp(int(image.timestamp)+60*60*2).strftime('%Y-%m-%d %H:%M:%S')
    print(image.pathRGB)

    return render_template('dashBoard.html', title='DashBoard',value=value, images=images,date=date,camera=camera, image=image, aula=aula)
    
    

@app.route('/graphs', methods=['GET'])
def graphoard():
    camera_id = request.args.get("camera")
    if camera_id is None:
        print("Invalid Request, Please insert Camera")
        return "Fallito"
    else:
        if(not db.session().query(Camera).filter_by(id = camera_id).count()):
            return "Fallito"
    camera = db.session.query(Camera).filter_by(id=camera_id).first()
    aula = db.session.query(Aula).filter_by(id=camera.aula_id).first()
    return render_template('graphs.html', title='Graphs', camera = camera,aula = aula)
    
    
    
@app.route('/uploadPhoto/<int:aula_id>/<string:camera>', methods=['POST'])
def uploadPhoto(aula_id,camera):
   camera = db.session.query(Camera).filter_by(aula_id=aula_id,nome=camera).first()
   if(db.session.query(Photo).filter_by(camera_id=camera.id).count() > MAX_STORED_PHOTOS):
      toRmv = db.session.query(Photo).filter_by(camera_id=camera.id).order_by(Photo.timestamp).first()
      os.remove("app/static/"+toRmv.pathRGB)
      os.remove("app/static/"+toRmv.pathPRD)
      db.session.query(Photo).filter_by(id=toRmv.id).delete()


   file = request.files['image']
   pic = Photo(pathPRD=str(aula_id)+'/'+camera.nome+'/PRDPhotos/' + file.filename[:-4] + '.jpg',
               pathRGB=str(aula_id)+'/'+camera.nome+'/RGBPhotos/' + file.filename,
               camera_id = camera.id,
               timestamp=file.filename[:-4])
   db.session.add(pic)
   db.session.commit()
   file.save('app/static/'+str(aula_id)+'/'+camera.nome+'/RGBPhotos/'+file.filename)
   subprocess.call('sh /home/local/STUDENTI/luca.rispoli/master/flaskTest/script.sh ' + file.filename + ' ' + str(aula_id) + ' ' + camera.nome, shell=True)
   return 'TUTTO OK'

    
@app.route('/aggiungi_aula/<int:aula_id>/<string:aula_nome>')
def aggiungi_aula(aula_id,aula_nome):
    aula = Aula(id=aula_id,nome=aula_nome)
    db.session.add(aula)
    db.session.commit()
    subprocess.call('sh /home/local/STUDENTI/luca.rispoli/master/flaskTest/creaAula.sh ' + str(aula_id), shell=True)
    return 'TUTTO OK '
    
@app.route('/aggiungi_camera/<int:aula_id>/<string:camera_nome>')
def aggiungi_camera(aula_id,camera_nome):
    aula=aula_id
    nomeCamera=camera_nome
    camera = Camera(nome=nomeCamera,aula_id=aula,ip="10.0.0.36")
    db.session.add(camera)
    db.session.commit()
    subprocess.call('sh /home/local/STUDENTI/luca.rispoli/master/flaskTest/creaCamera.sh ' + str(aula) + ' ' + nomeCamera, shell=True)
    return 'TUTTO OK'
    

def check_subject(timestamp,aula):
    
    data = datetime.utcfromtimestamp(timestamp).strftime('%d/%m/%Y')
    timestampGiorno = time.mktime(datetime.strptime(data, "%d/%m/%Y").timetuple())
    codiceAula = aula[0:1] + "."+aula[1:]
    url = 'https://www.unibo.it/UniboWeb/Utils/OrarioLezioni/RestService.aspx?SearchType=OccupazioneAule&Edificio=EXZUCC_'+str(codiceAula)+'&Data='+str(data)
    resp = requests.get(url)
    with open('schedule.xml', 'wb') as f: 
        f.write(resp.content) 
    tree = ET.parse('schedule.xml') 
    orario = tree.getroot()
    
    for evento in orario:
        for child in evento:
            if(child.tag == "Descrizione"):
                subj = child.text
            if(child.tag == "OraInizio"):
                oraInizio = timestampGiorno + int(child.text[0:2])*3600
            if(child.tag == "OraFine"):
                oraFine = timestampGiorno + int(child.text[0:2])*3600
                if(timestamp <= oraFine and timestamp >= oraInizio):
                    return subj
    return "Free Room"
                
@app.route('/subjects')
def fetch_subjects():
    date = request.args.get("dataInizio")
    final = request.args.get("dataFine")
    dataInizio = datetime.strptime(date, "%Y-%m-%d").strftime("%d/%m/%Y")
    timeInizio = time.mktime(datetime.strptime(dataInizio, "%d/%m/%Y").timetuple()) + 86400
    dataFine = datetime.strptime(final, "%Y-%m-%d").strftime("%d/%m/%Y")
    timeFine = time.mktime(datetime.strptime(dataFine, "%d/%m/%Y").timetuple()) + 86400
    aula = request.args.get("aula")
    codiceAula = aula[0:1] + "."+aula[1:]
    materie = list()
    while timeInizio <= timeFine:
        dataInizio = datetime.utcfromtimestamp(timeInizio).strftime('%d/%m/%Y')
        url = 'https://www.unibo.it/UniboWeb/Utils/OrarioLezioni/RestService.aspx?SearchType=OccupazioneAule&Edificio=EXZUCC_'+codiceAula+'&Data='+dataInizio
        resp = requests.get(url)
        with open('schedule.xml', 'wb') as f: 
            f.write(resp.content) 
        tree = ET.parse('schedule.xml') 
        orario = tree.getroot()
        for evento in orario:
            for child in evento:
                if(child.tag == "Descrizione"):
                    materie.append(child.text)
        timeInizio = timeInizio + 86400
    mylist = list(dict.fromkeys(materie))
    message = {
            'status': 200,
            'message': 'TUTTO OK',
            'subjects': mylist,
            }
    return jsonify(message)


@app.route('/fetchData', methods=['GET'])
#@login_required
def fetch_readings():
    start = request.args.get("start")
    end = request.args.get("end")
    camera_id = request.args.get("camera_id")
    materia = request.args.get("materia")
    i = 0
    days = 0
    aula_id = db.session().query(Camera).filter_by(id=camera_id).first().aula_id    
    camera = db.session().query(Camera).filter_by(id=camera_id).first()
    data = dict()
    y0 = start[0:4]
    m0 = start[5:7]
    d0 = start[8:10]
    y1 = end[0:4]
    m1 = end[5:7]
    d1 = end[8:10]
    
    date1 = datetime(int(y1), int(m1), int(d1))
    date0 = datetime(int(y0), int(m0), int(d0))
    delta = date1 - date0
    days = delta.days + 1
    startTime = calendar.timegm(date0.timetuple())
    # End of First hour of First Day
    base = startTime + 32400-7200
    # End of First Day
    baseDay = startTime + 86400-7200
    endTime = calendar.timegm(date1.timetuple()) + 86400
    if (days == 1):
        with open("app/static/"+ str(aula_id) +"/"+ str(camera.nome) +"/letture.csv", "rt") as f:
            scoresTime = list()
            for row in reversed(list(csv.reader(f))):
                if (int(row[0]) >= startTime and int(row[0]) <= endTime):
                    data[int(row[0])] = row[1]
                    scoresTime.append(int(row[0]))
            maxP = 0
            j = 0
            h = 8
            scoresResult = list()
            scoresInterval = list()
            for timestamp in reversed(scoresTime):
                if (int(timestamp) <= base):
                    if (int(data[timestamp]) > int(maxP)):
                        maxP = int(data[timestamp])
                else:
                    base = base + 3600
                    if (materia == "Tutte" or check_subject(int(timestamp) - 3600,str(aula_id)) == materia):
                        scoresResult.append(maxP)
                    else:
                        scoresResult.append(0)
                    scoresInterval.append(h)
                    h = h + 1
                    maxP = int(data[timestamp])

            if (materia == "Tutte" or check_subject(int(timestamp) - 3600,str(aula_id)) == materia):
                scoresResult.append(maxP)
            else:
                 scoresResult.append(0)
            scoresInterval.append(h)        

            message = {
                'status': 200,
                'message': 'TUTTO OK',
                'scoresResult': scoresResult,
                'scoresInterval': scoresInterval
            }

    elif (days <= 31000):
        with open("app/static/"+ str(aula_id) +"/"+ str(camera.nome) +"/letture.csv", "rt") as f:
            scoresTime = list()
            for row in reversed(list(csv.reader(f))):
                if (int(row[0]) >= startTime and int(row[0]) <= endTime):
                    data[int(row[0])] = row[1]
                    scoresTime.append(int(row[0]))
            maxP = 0
            j = 0
            d = int(d0)
            m = int(m0)
            year = int(y0)
            scoresResult = list()
            scoresResultDay = list()
            scoresIntervalDay = list()
            scoresTimestamps = list()

            for timestamp in reversed(scoresTime):
                if (int(timestamp) <= base):
                    if (int(data[timestamp]) > int(maxP)):
                        maxP = int(data[timestamp])
                else:
                    if (materia == "Tutte" or check_subject(int(timestamp) - 3600,str(aula_id)) == materia):
                        scoresResult.append(maxP)
                    else:
                        scoresResult.append(0)
                    scoresTimestamps.append(timestamp)
                    while int(timestamp) >= base:
                        base = base + 3600
                    maxP = int(data[timestamp])

            if (materia == "Tutte" or check_subject(int(timestamp) - 3600,str(aula_id)) == materia):
                scoresResult.append(maxP)
            else:
                 scoresResult.append(0)
            scoresTimestamps.append(timestamp)

            i = 0
            k = 0
            
            media = 0.0
            
            while i < len(scoresResult):
            #for y in scoresResult:
                if (int(scoresTimestamps[i]) <= baseDay):
                    if(scoresResult[i] == 0 and not materia == "Tutte"):
                        i = i + 1
                    else:
                        media = float(media) + float(scoresResult[i])
                        k = k + 1
                        i = i + 1
                else:
                    if (k == 1):
                        scoresIntervalDay.append(str(d)+"/"+str(m))
                        scoresResultDay.append(0)
                    else:
                        if (k > 0):
                            media = float(media) / float(k)
                        else:
                            media = 0.0
                        
                        scoresIntervalDay.append(str(d)+"/"+str(m))
                        scoresResultDay.append(media)                        
                        media = float(scoresResult[i])
                        k = 1
                    
                    if (m < 12):
                        date1 = datetime(year, m + 1, 1)
                        date0 = datetime(year, m, 1)
                        delta = date1 - date0
                        days = delta.days
                        if (d < days):
                            d = d + 1
                        else:
                            d = 1
                            m = m + 1
                    else:
                        date1 = datetime(year + 1, 1, 1)
                        date0 = datetime(year, m, 1)
                        delta = date1 - date0
                        days = delta.days
                        if (d < days):
                            d = d + 1
                        else:
                            d = 1
                            m = 1
                    #while int(scoresTimestamps[i]) >= baseDay:
                    baseDay = baseDay + 86400
                    
                #i = i + 1
            media = float(media) / float(k)
            scoresIntervalDay.append(str(d)+"/"+str(m))
            scoresResultDay.append(media)
        message = {
            'status': 200,
            'message': 'TUTTO OK',
            'scoresResult': scoresResultDay,
            'scoresInterval': scoresIntervalDay
        }

    return jsonify(message)
    
    
    
@app.route('/fetchCameras', methods=['GET'])
@login_required
def fetch_cameras():
    aula_id = request.args.get("aula")
    cameras = db.session().query(Camera).filter_by(aula_id=aula_id).with_entities(Camera.id,Camera.nome).all()
    message = {
        'status': 200,
        'message': 'TUTTO OK',
        'scores': cameras
    }
    return jsonify(message)






	
