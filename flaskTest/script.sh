#!bin/bash

cd /home/local/STUDENTI/luca.rispoli/darknet

aula=$2
tempo=${1%.*}

people=`./darknet detect cfg/yolov3.cfg yolov3.weights /home/local/STUDENTI/luca.rispoli/master/flaskTest/app/static/$aula/$3/RGBPhotos/$1 -thresh 0.25 | grep person | wc -l`
mv /home/local/STUDENTI/luca.rispoli/darknet/predictions.jpg /home/local/STUDENTI/luca.rispoli/master/flaskTest/app/static/$aula/$3/PRDPhotos/$tempo.jpg

echo $tempo,$people >> /home/local/STUDENTI/luca.rispoli/master/flaskTest/app/static/$aula/$3/letture.csv

exit
