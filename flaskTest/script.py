import requests
from app import db
from app.models import Camera
import subprocess, platform
import time
import os
from collections import defaultdict

chat_id = "-1001199644435"
TOKEN = "877368637:AAFliVdOLkDO_4m_5jzd1ukd96NdTGUsbKw"
url = "https://api.telegram.org/bot877368637:AAFliVdOLkDO_4m_5jzd1ukd96NdTGUsbKw/sendMessage"
tries = defaultdict()

def ping(host):
    # Ping -n per Windows , Ping -c per LINUX
    ping_str = "-n 1" if  platform.system().lower()=="windows" else "-c 1"
    args = "ping " + " " + ping_str + " " + host
    devnull = open("/dev/null","w")	
    need_sh = False if  platform.system().lower()=="windows" else True
    return subprocess.call(args, shell=need_sh, stdout=devnull)
    
def send_mess(text):
    params = {'chat_id':chat_id, 'text': text}
    response = requests.post(url, data=params)
    return response

while(1):
    camere = db.session().query(Camera).all()
    
    for camera in camere:
        if(camera.ip != None):
            print(camera.state)
            if(camera.ip not in tries):
                tries[camera.ip] = 0
            response = ping(camera.ip)
            if(response and camera.state != "OFF"):
                tries[camera.ip] += 1
                if(tries[camera.ip] == 10):
                    print(send_mess("La camera IP: "+ camera.ip + " Modello: "+camera.nome+ " E' offline").status_code)
                    db.session().query(Camera).filter_by(id=camera.id).update({"state": "OFF"})
                    db.session().commit()
                    tries[camera.ip] = 0
                else:
                    print(str(tries[camera.ip]))
            elif(not response and camera.state != "ON"):
                tries[camera.ip] = 0
                print(send_mess("La camera IP: "+ camera.ip + " Modello: "+camera.nome+ " E' tornata Online").status_code)
                db.session().query(Camera).filter_by(id=camera.id).update({"state": "ON"})
                db.session().commit()
            elif(not response):
                tries[camera.ip] = 0
            
            print("La camera con IP"+ str(camera.ip) + "Ha tentativi: " + str(tries[camera.ip]) + "Stato = " + str(camera.state) + "Ping: "+str(response))
            time.sleep(60)    

    
    
