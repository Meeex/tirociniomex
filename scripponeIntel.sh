#!/bin/bash



#non trova la libreria pyrealsense2
if ping -q -c 1 -W 1 8.8.8.8 >/dev/null; then
	export PYTHONPATH=$PYTHONPATH:/usr/local/lib
	
	cd /home/hotsquad/Scrivania/librealsense/build/examples/save-to-disk/
	while [ ! -f ./rs-save-to-disk-output-Depth.png ]
	do
	  ./rs-save-to-disk
	  sleep 1
	done
	tempo="$(date +%s)"

	#python3 /home/hotsquad/Scrivania/count_people/rotate.py
	mogrify -rotate 180 ./*-Depth.png
	mogrify -rotate 180 ./*-Color.png

	mv ./*-Depth.png /home/hotsquad/Scrivania/count_people/Depthphotos/$tempo.png
	mv ./*-Color.png /home/hotsquad/Scrivania/count_people/RGBphotos/$tempo.png

	cd /home/hotsquad/darknet-nnpack/
	people=`./darknet detect ./cfg/yolov3-tiny.cfg ./yolov3-tiny.weights ~/Scrivania/count_people/RGBphotos/$tempo.png -thresh 0.25 | grep person | wc -l`
	mv ./prediction* /home/hotsquad/Scrivania/count_people/Predictions/$tempo.png
	cd /home/hotsquad/Scrivania/count_people/
	echo $tempo,$people >> ~/Scrivania/count_people/letture.csv
else 
	echo $(date)" Errore connessione WiFi" >> errorLog.txt
	sudo reboot -n	
fi
