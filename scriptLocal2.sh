#!bin/bash

cd /home/hotsquad/Desktop/Testsh


for f in *; do
	echo $f

    cd /home/hotsquad/darknet/
    
    #big yolo intel
    people=`./darknet detect cfg/yolov3.cfg yolov3.weights /home/hotsquad/Desktop/Testsh/$f/Intel/*.png -thresh 0.25 | grep person | wc -l`
    mv ./predictions.jpg /home/hotsquad/Desktop/Testsh/$f/Intel/yolo-predictions-$people.jpg
    
    #tiny yolo intel
    people=`./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights /home/hotsquad/Desktop/Testsh/$f/Intel/*.png -thresh 0.25 | grep person | wc -l`
    mv ./predictions.jpg /home/hotsquad/Desktop/Testsh/$f/Intel/yolo-tiny-predictions-$people.jpg

    #big yolo kinect
    people=`./darknet detect cfg/yolov3.cfg yolov3.weights /home/hotsquad/Desktop/Testsh/$f/Kinect/*.ppm -thresh 0.25 | grep person | wc -l`
    mv ./predictions.jpg /home/hotsquad/Desktop/Testsh/$f/Kinect/yolo-predictions-$people.jpg

    #tiny yolo kinect
    people=`./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights /home/hotsquad/Desktop/Testsh/$f/Kinect/*.ppm -thresh 0.25 | grep person | wc -l`
    mv ./predictions.jpg /home/hotsquad/Desktop/Testsh/$f/Kinect/yolo-tiny-predictions-$people.jpg
    
done
