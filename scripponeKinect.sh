#!/bin/bash


MAX_PHOTOS_STORED=3360
#while true; do
	if ping -q -c 1 -W 1 8.8.8.8 >/dev/null; then
		cd /home/pi/darknet-nnpack/
		echo "la connessione funziona" >> check.txt
		echo "crontab parte" >> /home/pi/darknet-nnpack/check.txt
		sudo freenect-regtest
		if [ -f ./*_rgb.ppm ]; then
			tempo="$(date +%s)"
			echo "freenect funziona" >> check.txt
			sudo mogrify -rotate 180 ./*ppm
			sudo mogrify -rotate 180 ./*pgm
			people=`./darknet detect ./cfg/yolov3-tiny.cfg ./yolov3-tiny.weights ./*_rgb.ppm -thresh 0.25 | grep person | wc -l`
			echo "detect funziona" >> check.txt
			mv ./registration_test_depth_mm* ./depth/MM-$tempo.pgm
			mv ./registration_test_depth_re* ./depth/REG-$tempo.pgm
			mv ./*_rgb.ppm ./RGBphotos/$tempo.ppm
			mv ./predictions.png ./predictions/$tempo.png
			echo "mv funziona" >> check.txt
			count=`ls -l ./predictions/*.png | wc -l`
			if [ $count -gt $MAX_PHOTOS_STORED ]; then
				#delete old predictions
				toDel="$(ls -t ./predictions/ | tail -1)"
				rm ./predictions/$toDel
				#delete old rgb
				toDel="$(ls -t ./RGBphotos/ | tail -1)"
				rm ./RGBphotos/$toDel
				#delete old depth
				toDel="$(ls -t ./deph/ | tail -1)"
				rm ./depth/$toDel
				toDel="$(ls -t ./deph/ | tail -1)"
				rm ./depth/$toDel
			else
				echo "ancora spazio"
			fi
			echo $tempo,$people >> letture.csv
		else
			echo "Errore Kinect" >> errorLog.txt
			sudo reboot -n
		fi
	else
		echo "Errore connessione WiFi" >> errorLog.txt
		sudo reboot -n
	fi
#	sleep 5m
#done
